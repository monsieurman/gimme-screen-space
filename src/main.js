// This is the class used by medium sites for the top bar.
const metabar = document.querySelector('.metabar');

if (metabar) {
    console.info('[Gimme screen space] improving your medium experience.');
    const bottomBar = document.querySelector('.u-zIndexMetabar.u-fixed.u-bottom0');
    window.onscroll = () => scrollFunction(metabar, bottomBar);
}

/**
 * @param {HTMLElement} [topBar]
 * @param {HTMLElement} [bottomBar]
 */
function scrollFunction(topBar, bottomBar) {
    if (scrolledDown()) {
        topBar.classList.add('gcs-top-bar-hidden');
        bottomBar.classList.add('gcs-bottom-bar-hidden');
    } else {
        topBar.classList.remove('gcs-top-bar-hidden');
        bottomBar.classList.remove('gcs-bottom-bar-hidden');
    }
}

/**
 * @return {boolean}
 */
function scrolledDown() {
    return document.body.scrollTop > 20 || document.documentElement.scrollTop > 20;
}

